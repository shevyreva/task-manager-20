package ru.t1.shevyreva.tm.command.system;

public class ApplicationExitCommand extends AbstractSystemCommand {

    private final String DESCRIPTION = "Close application.";

    private final String NAME = "exit";

    // private final String ARGUMENT = "-v";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
