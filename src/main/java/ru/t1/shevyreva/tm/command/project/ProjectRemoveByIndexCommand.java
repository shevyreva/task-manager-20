package ru.t1.shevyreva.tm.command.project;

import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Remove project by Index.";

    private final String NAME = "project-remove-by-index";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        getProjectService().removeByIndex(index);
    }

}
