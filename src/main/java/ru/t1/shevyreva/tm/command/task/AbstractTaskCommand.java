package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    public IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[TASK ID]: " + task.getId());
        System.out.println("[TASK NAME]: " + task.getName());
        System.out.println("[TASK DESCRIPTION]: " + task.getDescription());
        System.out.println("[TASK STATUS]: " + Status.toName(task.getStatus()));
        System.out.println("[PROJECT ID]: " + task.getProjectId());
    }

}
