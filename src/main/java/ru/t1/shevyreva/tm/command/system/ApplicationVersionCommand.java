package ru.t1.shevyreva.tm.command.system;

import ru.t1.shevyreva.tm.constant.ApplicationConst;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    private final String DESCRIPTION = "Show program version.";

    private final String NAME = "version";

    private final String ARGUMENT = "-v";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.printf("%s.%s.%s \n", ApplicationConst.APP_MAJOR_VERSION, ApplicationConst.APP_MINOR_VERSION, ApplicationConst.APP_FIXES_VERSION);
    }

}
