package ru.t1.shevyreva.tm.command.project;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Create new project.";

    private final String NAME = "project-create";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }
}
