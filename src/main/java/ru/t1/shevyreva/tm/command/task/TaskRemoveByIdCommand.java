package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Remove task by Id.";

    private final String NAME = "task-remove-by-id";


    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().removeById(userId, id);
    }

}
