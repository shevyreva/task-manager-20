package ru.t1.shevyreva.tm.command.task;

public class TaskClearCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Clear task.";

    private final String NAME = "task-clear";


    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }

}
