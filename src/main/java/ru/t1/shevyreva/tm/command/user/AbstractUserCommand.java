package ru.t1.shevyreva.tm.command.user;

import ru.t1.shevyreva.tm.api.service.IAuthService;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("Login: " + user.getLogin());
        System.out.println("Id: " + user.getId());
    }

}
