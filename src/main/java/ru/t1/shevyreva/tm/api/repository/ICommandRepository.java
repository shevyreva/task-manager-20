package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

    void add(AbstractCommand command);

    Collection<AbstractCommand> getTerminalCommands();

}
