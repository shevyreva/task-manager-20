package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project, IProjectRepository> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project updateById(String userId, String id, String name, String description);

    Project changeProjectStatusByIndex(String userId, Integer Index, Status status);

    Project changeProjectStatusById(String userId, String id, Status status);

}
