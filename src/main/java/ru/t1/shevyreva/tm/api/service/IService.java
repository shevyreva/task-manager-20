package ru.t1.shevyreva.tm.api.service;

import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    void clear();

    M findOneById(String Id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String Id);

    void removeByIndex(Integer index);

    boolean existsById(String id);

    Integer getSize();

}
