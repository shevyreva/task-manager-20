package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.api.service.IUserOwnedService;
import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.IndexEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M, R> {

    public AbstractUserOwnedService(R repository) {
        super(repository);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.add(userId, model);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.remove(userId, model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(userId, id);
        return model;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() <= index) throw new IndexEmptyException();
        final M model = repository.findOneByIndex(userId, index);
        return model;
    }

    @Override
    public Integer getSize(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Override
    public void removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() <= index) throw new IndexEmptyException();
        repository.removeByIndex(userId, index);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsById(userId, id);
    }

}
