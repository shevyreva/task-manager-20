package ru.t1.shevyreva.tm.enumerated;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return this.displayName;
    }

}
